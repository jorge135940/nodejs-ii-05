const { Router } = require('express');
const fechaHoraGet = require('../controllers/fechaYHoraController');

const router = Router();

router.get('/', fechaHoraGet)

module.exports = router;